**Проект "структура подразделения"**

## Разворачивание проекта

1. Скачать и установить docker **https://www.docker.com/get-started**
2. В терминале запустить команду "docker-compose up"
3. Открыть в браузере **http://localhost/**

## Архитектура

1. Backend: Node.js, express
2. Frontend: Vue
3. DB: mongo