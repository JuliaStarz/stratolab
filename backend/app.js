const express = require('express');
const bodyParser = require('body-parser');
const uuid = require('uuid/v4');

const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const ObjectId = mongodb.ObjectId;

const PORT = 8080;
const MONGO_URL = process.env['DB_URL'];
const POLL_ENDPOINT = '/api/department/poll';

const app = express();

app.use(bodyParser.json());

function publishChanges() {
    execute(collection => collection
        .find({})
        .toArray()
        .then((res) => poll.publish(res)), 
        false)
}

const poll = {
    QUEUE_SIZE: 50,
    queue: [],

    add(response) {
        if (this.queue.length == this.QUEUE_SIZE) {
            this.queue.shift();
        }    
        this.queue.push(response);
    },

    publish(data) {
        const processingQueue = this.queue;
        this.queue = [];
        processingQueue.forEach(response => response.json(data));
    }
}

app.get(POLL_ENDPOINT, (req, res) => {
    poll.add(res);
})

app.post('/api/department', (req, response) => {
    execute((collection) => collection.insertOne({
            name: req.query.name,
            employees: []
        }).then((res) => response.json(res.insertedId), err => response.json(err))
    );
});

app.put('/api/department/:id', (req, response) => {
    execute((collection) => collection.updateOne({
            _id: ObjectId(req.params.id)
        },
        {
            $set: {
                name: req.query.name
            }
        }).then(() => response.json(), err => response.json(err))
    );
});

app.get('/api/department', (req, response) => {
    execute(collection => collection
        .find({})
        .toArray()
        .then(res => response.json(res), err => response.json(err)),
    false);
});

app.delete('/api/department/:id', (req, response) => {
    execute((collection) => collection
        .deleteOne({_id: ObjectId(req.params.id)})
        .then((res) => response.json(), err => response.json(err))
    )
});

app.delete('/api/department/:id/employee/:employeeid', (req, response) => {
    execute((collection) => collection
        .updateOne(
            {_id: ObjectId(req.params.id)}, 
            {$pull: {employees: {id: req.params.employeeid}}}
        )
        .then(res => response.json(), err => response.json(err))
    )
});

app.post('/api/department/:id/employee', (req, response) => {
    postOrPut( 
        response, 
        {_id: ObjectId(req.params.id)},
        {$push: {'employees': {id: uuid(), name: req.query.name}}}
    );
});

app.put('/api/department/:id/employee/:employeeid', (req, response) => {
    postOrPut(
        response, 
        {_id: ObjectId(req.params.id), 'employees.id': req.params.employeeid},
        {$set: {'employees.$': {id: req.params.employeeid, name: req.query.name}}}
    );
});

app.listen(PORT);
console.log(`App listens on port ${PORT}`)

function postOrPut(response, selector, updater) {
    execute((collection) => collection.updateOne(selector, updater)
            .then(res => response.json(), err => response.json(err))
    );
};

function execute(handler, publish = true) {
    client = new MongoClient(MONGO_URL, { useNewUrlParser: true });
    client.connect(async() => {
        const db = client.db('mystore');
        const collection = db.collection('department');  
        await handler(collection);
        client.close();
        if (publish) {
            publishChanges();    
        }
    });
}
