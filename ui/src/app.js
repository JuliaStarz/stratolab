import Vue from "vue";
import DepartmentList from "./components/department-list.vue";

import 'vue-awesome/icons/edit';
import 'vue-awesome/icons/trash-alt';
import Icon from 'vue-awesome/components/Icon.vue'
Vue.component('icon', Icon)

new Vue({
  render: h => h(DepartmentList)
}).$mount('#main')